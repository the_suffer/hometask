﻿using System;


namespace Zadanie1
{
    class Program
    {
        static int Add(int x, int y)
        {
            int add = x + y;
            Console.WriteLine("Результат от операции сложение: "+add);
            return add;
        }

        static int Sub(int x, int y)
        {
            int sub = x - y;
            Console.WriteLine("Результат от операции вычитание: " + sub);
            return sub;
        }

        static int Mult (int x, int y)
        {
            int mult = x * y;
            Console.WriteLine("Результат от операции умножение: " + mult);
            return mult;
        }

        static double Del (double x, double y)
        {
            if (y==0)
            {
                Console.WriteLine("Ошибка! На 0 делить нельзя. Выберите дугое действие или введите другие числа.");
                return 0;
            }

            else
            { 
                double del = x / y;
                Console.WriteLine("Результат от операции деление: " + del);
                return del;
            }
        }

        static int Step(int x, int y)
        {
            int step = Convert.ToInt32(Math.Pow(x,y));
            Console.WriteLine("Результат от операции возведение в степень: " + step);
            return step;
        }

        static double Sqrt(double x, double y)
        {
            if ((x < 0) || (y<0))
            {
                Console.WriteLine("Ошибка! Нельзя извлечь корень из отрицательного числа. Выберите дугое действие или введите другие числа.");
                return 0;
            }

            else
            {
                double sq = Math.Sqrt(x);
                double sq1 = Math.Sqrt(y);
                Console.WriteLine("Результат от операции извлечение корня: \nПервое число:" + sq + "\nВторое число:" +sq1);
                return sq;
            }
        }

        static double Ost(double x, double y)
        {
            if (y == 0)
            {
                Console.WriteLine("Ошибка! На 0 делить нельзя. Выберите дугое действие или введите другие числа.");
                return 0;
            }

            else
            {
                double ost = x % y;
                Console.WriteLine("Результат от операции остаток от деления: " + ost);
                return ost;
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Приложение калькулятор");
            input:
            Console.WriteLine("Введите числа, над которыми будет выполнена операция:");
            input1:
            Console.WriteLine("Первое число:");
            var x = Console.ReadLine();

            if (System.Text.RegularExpressions.Regex.IsMatch(x, "^\\d{1}$") ){

                Convert.ToInt32(x);
                
            }

            else
            {
                Console.WriteLine("Некорректное значение: '" + x + "'. Пожалуйста, введите целое число.");
                goto input1;
            }
            
            input2:
            Console.WriteLine("Второе число:");
            var y = Console.ReadLine();

            if (System.Text.RegularExpressions.Regex.IsMatch(y, "^\\d{1}$"))
            {

                Convert.ToInt32(y);

            }

            else
            {
                Console.WriteLine("Некорректное значение: '" + y + "'. Пожалуйста, введите целое число.");
                goto input2;
            }

            choice:
            Console.WriteLine("Выберите действие:\n1.Сложение\n2.Вычитание\n3.Умножение\n4.Деление\n5.Возведение в степень\n6.Извлечение корня\n7.Остаток от деления\n8.Выход");
            var choice = Console.ReadLine();

            if (System.Text.RegularExpressions.Regex.IsMatch(choice, "^\\d{1}$"))
            {

                Convert.ToInt32(choice);

            }

            else
            {
                Console.WriteLine("Некорректное значение: '" + choice + "'. Пожалуйста, введите целое число из списка.");
                goto choice;
            }


           
            switch (Convert.ToInt32(choice))
            {
                case 1:
                    var add = Add(Convert.ToInt32(x), Convert.ToInt32(y));
                    ch:
                    Console.WriteLine("Хотите продолжить ? (1 - да, 0 - нет)");
                    var ch = Console.ReadLine();
                    switch (Convert.ToInt32(ch)) {
                        case 1:
                            goto input;
                        case 0:
                            Console.WriteLine("Завершение программы.");
                            break;
                        default:
                            Console.WriteLine("Вы ввели несуществующий вариант. Пожалуйста, выберите один из предложенных.");
                            goto ch;
                    }
                    break;
                case 2:
                    var sub = Sub(Convert.ToInt32(x), Convert.ToInt32(y));
                    ch1:
                    Console.WriteLine("Хотите продолжить ? (1 - да, 0 - нет)");
                    var ch1 = Console.ReadLine();
                    switch (Convert.ToInt32(ch1))
                    {
                        case 1:
                            goto input;
                        case 0:
                            Console.WriteLine("Завершение программы.");
                            break;
                        default:
                            Console.WriteLine("Вы ввели несуществующий вариант. Пожалуйста, выберите один из предложенных.");
                            goto ch1;
                    }
                    break;
                case 3:
                    var mult = Mult(Convert.ToInt32(x), Convert.ToInt32(y));
                    ch2:
                    Console.WriteLine("Хотите продолжить ? (1 - да, 0 - нет)");
                    var ch2 = Console.ReadLine();
                    switch (Convert.ToInt32(ch2))
                    {
                        case 1:
                            goto input;
                        case 0:
                            Console.WriteLine("Завершение программы.");
                            break;
                        default:
                            Console.WriteLine("Вы ввели несуществующий вариант. Пожалуйста, выберите один из предложенных.");
                            goto ch2;
                    }
                    break;
                case 4:
                    double del = Del(Convert.ToDouble(x), Convert.ToDouble(y));
                    if (del == 0)
                    {
                        goto input;
                    }
                    else
                    {
                    ch3:
                        Console.WriteLine("Хотите продолжить ? (1 - да, 0 - нет)");
                        var ch3 = Console.ReadLine();
                        switch (Convert.ToInt32(ch3))
                        {
                            case 1:
                                goto input;
                            case 0:
                                Console.WriteLine("Завершение программы.");
                                break;
                            default:
                                Console.WriteLine("Вы ввели несуществующий вариант. Пожалуйста, выберите один из предложенных.");
                                goto ch3;
                        }
                    }
                        break;
                    
                case 5:
                    var step = Step(Convert.ToInt32(x), Convert.ToInt32(y));
                ch4:
                    Console.WriteLine("Хотите продолжить ? (1 - да, 0 - нет)");
                    var ch4 = Console.ReadLine();
                    switch (Convert.ToInt32(ch4))
                    {
                        case 1:
                            goto input;
                        case 0:
                            Console.WriteLine("Завершение программы.");
                            break;
                        default:
                            Console.WriteLine("Вы ввели несуществующий вариант. Пожалуйста, выберите один из предложенных.");
                            goto ch4;
                    }
                    break;
                case 6:
                    double sq = Sqrt(Convert.ToDouble(x), Convert.ToDouble(y));
                    if (sq == 0)
                    {
                        goto input;
                    }
                    else
                    {
                    ch5:
                        Console.WriteLine("Хотите продолжить ? (1 - да, 0 - нет)");
                        var ch5 = Console.ReadLine();
                        switch (Convert.ToInt32(ch5))
                        {
                            case 1:
                                goto input;
                            case 0:
                                Console.WriteLine("Завершение программы.");
                                break;
                            default:
                                Console.WriteLine("Вы ввели несуществующий вариант. Пожалуйста, выберите один из предложенных.");
                                goto ch5;
                        }
                    }
                    break;
                case 7:
                    double ost = Ost(Convert.ToDouble(x), Convert.ToDouble(y));
                    if (ost == 0)
                    {
                        goto input;
                    }
                    else
                    {
                    ch6:
                        Console.WriteLine("Хотите продолжить ? (1 - да, 0 - нет)");
                        var ch6 = Console.ReadLine();
                        switch (Convert.ToInt32(ch6))
                        {
                            case 1:
                                goto input;
                            case 0:
                                Console.WriteLine("Завершение программы.");
                                break;
                            default:
                                Console.WriteLine("Вы ввели несуществующий вариант. Пожалуйста, выберите один из предложенных.");
                                goto ch6;
                        }
                    }
                    break;
                case 8:
                    Console.WriteLine("Завершение программы.");
                    break;
                default:
                    Console.WriteLine("Вы ввели несуществующий вариант. Пожалуйста, выберите вариант из списка.");
                    goto choice;

            }

        }
    }
}
